require_relative './model/exceptions/here_error'
class DurationMapper
  SEGS_PER_MINUTE = 60
  def travel_duration(response)
    response = JSON.parse(response.body)
    begin
      response['routes'][0]['sections'][0]['summary']['duration'] / SEGS_PER_MINUTE
    rescue StandardError
      raise HereError
    end
  end
end

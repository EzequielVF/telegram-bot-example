class HereConnector
  include Routing
  URL_HERE = 'https://router.hereapi.com'.freeze

  def initialize(apikey)
    @apikey = apikey.nil? ? ENV['HERE_API_KEY'] : apikey
    @url = URL_HERE
    @connection = Faraday::Connection.new @url
  end

  def get_routes(origin, destination, transport)
    endpoint = "/v8/routes?apikey=#{@apikey}&destination=#{destination.point}&origin=#{origin.point}&return=summary&transportMode=#{transport}"
    @connection.get endpoint
  end
end

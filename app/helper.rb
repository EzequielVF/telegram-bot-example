class Helper
  def self.empty_or_nil(thing)
    thing.empty? || thing.nil?
  end
end

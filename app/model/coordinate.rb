require_relative './exceptions/parameter_error'
class Coordinate
  attr_accessor :latitud, :longitud

  def initialize(latitud, longitud)
    raise ParameterError if latitud.nil? || longitud.nil?

    @latitud = latitud
    @longitud = longitud
  end

  def point
    "#{@latitud},#{@longitud}"
  end
end

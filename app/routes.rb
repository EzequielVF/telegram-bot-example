require "#{File.dirname(__FILE__)}/../lib/routing"
require "#{File.dirname(__FILE__)}/../lib/version"
require "#{File.dirname(__FILE__)}/tv/series"
require "#{File.dirname(__FILE__)}/../app/model/coordinate"
require "#{File.dirname(__FILE__)}/../app/here_connector"
require "#{File.dirname(__FILE__)}/../app/duration_mapper"
require "#{File.dirname(__FILE__)}/../app/helper"
require_relative './model/exceptions/parameter_error'
require_relative './model/exceptions/here_error'

class Routes
  include Routing
  TRANSPORT = 'car'.freeze

  on_message '/start' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: "Hola, #{message.from.first_name}")
  end

  on_message_pattern %r{/say_hi (?<name>.*)} do |bot, message, args|
    bot.api.send_message(chat_id: message.chat.id, text: "Hola, #{args['name']}")
  end

  on_message '/stop' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: "Chau, #{message.from.username}")
  end

  on_message '/time' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: "La hora es, #{Time.now}")
  end

  on_message '/tv' do |bot, message|
    kb = [Tv::Series.all.map do |tv_serie|
      Telegram::Bot::Types::InlineKeyboardButton.new(text: tv_serie.name, callback_data: tv_serie.id.to_s)
    end]
    markup = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: kb)

    bot.api.send_message(chat_id: message.chat.id, text: 'Quien se queda con el trono?', reply_markup: markup)
  end

  on_message '/busqueda_centro' do |bot, message|
    kb = [
      Telegram::Bot::Types::KeyboardButton.new(text: 'Compartime tu ubicacion', request_location: true)
    ]
    markup = Telegram::Bot::Types::ReplyKeyboardMarkup.new(keyboard: kb)
    bot.api.send_message(chat_id: message.chat.id, text: 'Busqueda por ubicacion', reply_markup: markup)
  end

  on_location_response do |bot, message|
    response = "Ubicacion es Lat:#{message.location.latitude} - Long:#{message.location.longitude}"
    puts response
    bot.api.send_message(chat_id: message.chat.id, text: response)
  end

  on_response_to 'Quien se queda con el trono?' do |bot, message|
    response = Tv::Series.handle_response message.data
    bot.api.send_message(chat_id: message.message.chat.id, text: response)
  end

  on_message '/version' do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: Version.current)
  end

  on_message_pattern %r{/viaje (?<latitud_1>.*),(?<longitud_1>.*),(?<latitud_2>.*),(?<longitud_2>.*)} do |bot, message, args|
    # La parte de chequear si es algo es null o vacia y crear los objetos coordinate deberia estar dentro de un factory pero se me estaba haciendo tarde
    raise ParameterError if Helper.empty_or_nil(args['latitud_1']) || Helper.empty_or_nil(args['longitud_1']) || Helper.empty_or_nil(args['latitud_2']) || Helper.empty_or_nil(args['longitud_2'])

    origin = Coordinate.new(args['latitud_1'], args['longitud_1'])
    destination = Coordinate.new(args['latitud_2'], args['longitud_2'])
    mapper = DurationMapper.new
    connector = HereConnector.new(nil)
    duration = mapper.travel_duration(connector.get_routes(origin, destination, TRANSPORT))
    bot.api.send_message(chat_id: message.chat.id, text: "Vas a tardar #{duration} minutos en hacer tu viaje en auto")
  rescue HereError
    bot.api.send_message(chat_id: message.chat.id, text: 'Error, no se pudo encontrar una ruta')
  rescue StandardError
    bot.api.send_message(chat_id: message.chat.id, text: 'Error, verifica formato de latitud o longitud')
  end

  default do |bot, message|
    bot.api.send_message(chat_id: message.chat.id, text: 'Uh? No te entiendo! Me repetis la pregunta?')
  end
end

require 'spec_helper'
require 'web_mock'
require "#{File.dirname(__FILE__)}/../app/model/coordinate"
require "#{File.dirname(__FILE__)}/../app/here_connector"
require 'dotenv'

def intersect_api_call(apikey, origin, destination)
  stub_request(:get, "https://router.hereapi.com/v8/routes?apikey=#{apikey}&destination=#{destination.point}&origin=#{origin.point}&return=summary&transportMode=car")
    .with(
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: '', headers: {})
end

describe 'HereConnector' do
  let(:origin) { Coordinate.new(-34.67, -58.35) }
  let(:destination) { Coordinate.new(-34.61, -58.36) }
  let(:apikey) { 'fake' }
  let(:here) { HereConnector.new(apikey) }

  it 'when i call the here connector then the call did' do
    transport_mode = 'car'
    intersect_api_call(apikey, origin, destination)
    here.get_routes(origin, destination, transport_mode)
  end

  xit 'when i call the here connector then it response me' do
    WebMock.disable!
    Dotenv.load('.env')
    response = HereConnector.new(nil).get_routes(origin, destination, 'car')
    expect(response.status).to eq(200)
    WebMock.enable!
  end
end

require 'spec_helper'
require 'web_mock'
require "#{File.dirname(__FILE__)}/../app/model/coordinate"
require "#{File.dirname(__FILE__)}/../app/here_connector"
require "#{File.dirname(__FILE__)}/../app/duration_mapper"

def api_call_with_duration(apikey, origin, destination, duration)
  body = { "routes": [{ "id": 'fc68d31d-a184-480f-bf65-94ac8dea75be',
                        "sections": [{ "id": '99ff7321-527c-411c-99f6-a138a0f76c6f', "type": 'vehicle',
                                       "departure": { "time": '2023-10-28T14:14:32+02:00',
                                                      "place": { "type": 'place',
                                                                 "location": { "lat": 52.5309838, "lng": 13.3845671 },
                                                                 "originalLocation": { "lat": 52.5307999, "lng": 13.3847 } } },
                                       "arrival": { "time": '2023-10-28T14:16:16+02:00',
                                                    "place": { "type": 'place',
                                                               "location": { "lat": 52.5323264, "lng": 13.378874 },
                                                               "originalLocation": { "lat": 52.5323, "lng": 13.3789 } } },
                                       "summary": { "duration": duration, "length": 538, "baseDuration": 88 },
                                       "transport": { "mode": 'car' } }] }] }

  stub_request(:get, "https://router.hereapi.com/v8/routes?apikey=#{apikey}&destination=#{destination.point}&origin=#{origin.point}&return=summary&transportMode=car")
    .with(
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: body.to_json, headers: {})
end

def fail_api_call(apikey, origin, destination)
  body = { "notices": [{ "title": "Route calculation failed: Couldn't find a route.",
                         "code": 'noRouteFound',
                         "severity": 'critical' }],
           "routes": [] }

  stub_request(:get, "https://router.hereapi.com/v8/routes?apikey=#{apikey}&destination=#{destination.point}&origin=#{origin.point}&return=summary&transportMode=car")
    .with(
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: body.to_json, headers: {})
end

describe 'DurationMapper' do
  let(:origin) { Coordinate.new(52.5308, 13.3847) }
  let(:destination) { Coordinate.new(52.5323, 13.3789) }
  let(:apikey) { 'fake' }
  let(:here) { HereConnector.new(apikey) }

  it 'when i the service response duration 60 then i get 1 as duration' do
    transport_mode = 'car'
    mapper = DurationMapper.new
    api_call_with_duration(apikey, origin, destination, 60)
    response = here.get_routes(origin, destination, transport_mode)
    expect(mapper.travel_duration(response)).to eq 1
  end

  it 'when i the service response duration 65 then i get 1 as duration' do
    transport_mode = 'car'
    mapper = DurationMapper.new
    api_call_with_duration(apikey, origin, destination, 63)
    response = here.get_routes(origin, destination, transport_mode)
    expect(mapper.travel_duration(response)).to eq 1
  end

  it 'when the service response noRouteFound or no response then the bot response error' do
    transport_mode = 'car'
    mapper = DurationMapper.new
    fail_api_call(apikey, origin, destination)
    response = here.get_routes(origin, destination, transport_mode)
    expect { mapper.travel_duration(response) }.to raise_error(HereError)
  end
end

require 'spec_helper'
require "#{File.dirname(__FILE__)}/../app/model/coordinate"

describe 'Coordinate' do
  it 'valid?' do
    coordenada = Coordinate.new(64.57, 28.949)
    expect(coordenada.point).to eq '64.57,28.949'
  end

  it 'invalid?' do
    expect { Coordinate.new(64.57, nil) }.to raise_error(ParameterError)
  end
end

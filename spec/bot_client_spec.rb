require 'spec_helper'
require 'web_mock'
# Uncomment to use VCR
# require 'vcr_helper'

require "#{File.dirname(__FILE__)}/../app/bot_client"

def when_i_send_text(token, message_text)
  body = { "ok": true, "result": [{ "update_id": 693_981_718,
                                    "message": { "message_id": 11,
                                                 "from": { "id": 141_733_544, "is_bot": false, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "language_code": 'en' },
                                                 "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                                                 "date": 1_557_782_998, "text": message_text,
                                                 "entities": [{ "offset": 0, "length": 6, "type": 'bot_command' }] } }] }

  stub_request(:any, "https://api.telegram.org/bot#{token}/getUpdates")
    .to_return(body: body.to_json, status: 200, headers: { 'Content-Length' => 3 })
end

def when_i_send_keyboard_updates(token, message_text, inline_selection)
  body = {
    "ok": true, "result": [{
      "update_id": 866_033_907,
      "callback_query": { "id": '608740940475689651', "from": { "id": 141_733_544, "is_bot": false, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "language_code": 'en' },
                          "message": {
                            "message_id": 626,
                            "from": { "id": 715_612_264, "is_bot": true, "first_name": 'fiuba-memo2-prueba', "username": 'fiuba_memo2_bot' },
                            "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                            "date": 1_595_282_006,
                            "text": message_text,
                            "reply_markup": {
                              "inline_keyboard": [
                                [{ "text": 'Jon Snow', "callback_data": '1' }],
                                [{ "text": 'Daenerys Targaryen', "callback_data": '2' }],
                                [{ "text": 'Ned Stark', "callback_data": '3' }]
                              ]
                            }
                          },
                          "chat_instance": '2671782303129352872',
                          "data": inline_selection }
    }]
  }

  stub_request(:any, "https://api.telegram.org/bot#{token}/getUpdates")
    .to_return(body: body.to_json, status: 200, headers: { 'Content-Length' => 3 })
end

def when_here_answers(message_text, origin, destination)
  body = { "routes": [{ "id": 'fc68d31d-a184-480f-bf65-94ac8dea75be',
                        "sections": [{ "id": '99ff7321-527c-411c-99f6-a138a0f76c6f', "type": 'vehicle',
                                       "departure": { "time": '2023-10-28T14:14:32+02:00',
                                                      "place": { "type": 'place',
                                                                 "location": { "lat": 52.5309838, "lng": 13.3845671 },
                                                                 "originalLocation": { "lat": 52.5307999, "lng": 13.3847 } } },
                                       "arrival": { "time": '2023-10-28T14:16:16+02:00',
                                                    "place": { "type": 'place',
                                                               "location": { "lat": 52.5323264, "lng": 13.378874 },
                                                               "originalLocation": { "lat": 52.5323, "lng": 13.3789 } } },
                                       "summary": { "duration": message_text, "length": 538, "baseDuration": 88 },
                                       "transport": { "mode": 'car' } }] }] }

  stub_request(:get, "https://router.hereapi.com/v8/routes?apikey=&destination=#{destination.point}&origin=#{origin.point}&return=summary&transportMode=car")
    .with(
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: body.to_json, headers: {})
end

def when_here_answers_v2(message_text, _origin, destination)
  body = { "routes": [{ "id": 'fc68d31d-a184-480f-bf65-94ac8dea75be',
                        "sections": [{ "id": '99ff7321-527c-411c-99f6-a138a0f76c6f', "type": 'vehicle',
                                       "departure": { "time": '2023-10-28T14:14:32+02:00',
                                                      "place": { "type": 'place',
                                                                 "location": { "lat": 52.5309838, "lng": 13.3845671 },
                                                                 "originalLocation": { "lat": 52.5307999, "lng": 13.3847 } } },
                                       "arrival": { "time": '2023-10-28T14:16:16+02:00',
                                                    "place": { "type": 'place',
                                                               "location": { "lat": 52.5323264, "lng": 13.378874 },
                                                               "originalLocation": { "lat": 52.5323, "lng": 13.3789 } } },
                                       "summary": { "duration": message_text, "length": 538, "baseDuration": 88 },
                                       "transport": { "mode": 'car' } }] }] }

  stub_request(:get, "https://router.hereapi.com/v8/routes?apikey=&destination=#{destination.point}&origin=52.5308,&return=summary&transportMode=car")
    .with(
      headers: {
        'Accept' => '*/*',
        'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent' => 'Faraday v2.7.4'
      }
    )
    .to_return(status: 200, body: body.to_json, headers: {})
end

def then_i_get_text(token, message_text)
  body = { "ok": true,
           "result": { "message_id": 12,
                       "from": { "id": 715_612_264, "is_bot": true, "first_name": 'fiuba-memo2-prueba', "username": 'fiuba_memo2_bot' },
                       "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                       "date": 1_557_782_999, "text": message_text } }

  stub_request(:post, "https://api.telegram.org/bot#{token}/sendMessage")
    .with(
      body: { 'chat_id' => '141733544', 'text' => message_text }
    )
    .to_return(status: 200, body: body.to_json, headers: {})
end

def then_i_get_keyboard_message(token, message_text)
  body = { "ok": true,
           "result": { "message_id": 12,
                       "from": { "id": 715_612_264, "is_bot": true, "first_name": 'fiuba-memo2-prueba', "username": 'fiuba_memo2_bot' },
                       "chat": { "id": 141_733_544, "first_name": 'Emilio', "last_name": 'Gutter', "username": 'egutter', "type": 'private' },
                       "date": 1_557_782_999, "text": message_text } }

  stub_request(:post, "https://api.telegram.org/bot#{token}/sendMessage")
    .with(
      body: { 'chat_id' => '141733544',
              'reply_markup' => '{"inline_keyboard":[[{"text":"Jon Snow","callback_data":"1"},{"text":"Daenerys Targaryen","callback_data":"2"},{"text":"Ned Stark","callback_data":"3"}]]}',
              'text' => 'Quien se queda con el trono?' }
    )
    .to_return(status: 200, body: body.to_json, headers: {})
end

describe 'BotClient' do
  let(:destination) { Coordinate.new(52.5323, 13.3789) }
  let(:origin) { Coordinate.new(52.5308, 13.3847) }

  it 'should get a /version message and respond with current version' do
    token = 'fake_token'

    when_i_send_text(token, '/version')
    then_i_get_text(token, Version.current)

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get a /say_hi message and respond with Hola Emilio' do
    token = 'fake_token'

    when_i_send_text(token, '/say_hi Emilio')
    then_i_get_text(token, 'Hola, Emilio')

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get a /start message and respond with Hola' do
    token = 'fake_token'

    when_i_send_text(token, '/start')
    then_i_get_text(token, 'Hola, Emilio')

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get a /stop message and respond with Chau' do
    token = 'fake_token'

    when_i_send_text(token, '/stop')
    then_i_get_text(token, 'Chau, egutter')

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get a /tv message and respond with an inline keyboard' do
    token = 'fake_token'

    when_i_send_text(token, '/tv')
    then_i_get_keyboard_message(token, 'Quien se queda con el trono?')

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get a "Quien se queda con el trono?" message and respond with' do
    token = 'fake_token'

    when_i_send_keyboard_updates(token, 'Quien se queda con el trono?', '2')
    then_i_get_text(token, 'A mi también me encantan los dragones!')

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get an unknown message message and respond with Do not understand' do
    token = 'fake_token'

    when_i_send_text(token, '/unknown')
    then_i_get_text(token, 'Uh? No te entiendo! Me repetis la pregunta?')

    app = BotClient.new(token)

    app.run_once
  end

  it 'should get a /viaje with valid coords responds time from origin to destiny on car' do
    token = 'fake_token'
    when_here_answers(60, origin, destination)
    when_i_send_text(token, '/viaje 52.5308,13.3847,52.5323,13.3789')
    then_i_get_text(token, 'Vas a tardar 1 minutos en hacer tu viaje en auto')
    BotClient.new(token).run_once
  end

  it 'should get a /viaje with invalid parameters then respond error' do
    token = 'fake_token'
    when_i_send_text(token, '/viaje 52.5308,,52.5323,13.3789')
    then_i_get_text(token, 'Error, verifica formato de latitud o longitud')
    BotClient.new(token).run_once
  end
end
